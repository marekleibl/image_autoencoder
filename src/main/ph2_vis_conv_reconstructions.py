"""
Compare actual and reconstructed images using convolutional autoencoder.
"""

from matplotlib import pyplot as plt
import numpy as np
import logging
import setting
from model.conv_ae import ConvAE
from utils.utils import generate_batches

plt.style.use('dark_background')

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO, format='%(asctime)s:%(levelname)s:%(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')


def main():
    model_path = setting.convAE_dir

    model = ConvAE.load_model(model_path)
    batch_gen = generate_batches(**setting.cnn_hparams)

    # [batch, x, y, features]
    x = next(batch_gen)

    x_rec = model.reconstruct(x)

    for i in range(10):
        im = np.concatenate((x[i, :, :, 0], x_rec[i, :, :, 0]), axis=1)

        plt.imshow(1-im, cmap='gray')
        plt.title('Original (left) and reconstructed image (right).')
        plt.show()


if __name__ == '__main__':
    main()
