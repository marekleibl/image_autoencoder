"""
Train a stacked autoencoder.
"""

from os.path import join
import numpy as np
import logging

import setting
from model.conv_ae import ConvAE
from model.models_dict import models
from utils.utils import generate_batches, update_ema_loss

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO, format='%(asctime)s:%(levelname)s:%(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')


def get_latent_vectors(model, batch_gen, training_data_size):
    for i in range(training_data_size // setting.cnn_hparams['batch_size']):

        if i % 100 == 0:
            logger.info('{}/{}'.format(i, training_data_size))

        z = model.encode(next(batch_gen))
        yield z


def generate_batches_stackedae(z_data, batch_size):
    indexes = np.arange(z_data.shape[0])

    while True:
        ixs = np.random.choice(indexes, batch_size)
        yield z_data[ixs]


def train_stacked_autoencoder():
    # model_type = 'vanilla'
    # model_type = 'VAE'
    model_type = 'WAE'

    hparams = setting.cnn_hparams
    convae_path = setting.convAE_dir
    stackedae_path = join(setting.stackedAE_dir, model_type)

    # generate more if overfitting
    training_data_size = int(10 ** 4)

    # --- get model

    model = ConvAE.load_model(convae_path, **hparams)
    batch_gen = generate_batches(**setting.cnn_hparams)

    # --- create training data for latent autoencoder

    logger.info('creating training data ... ')

    z = get_latent_vectors(model, batch_gen, training_data_size)
    z = np.concatenate(tuple(z), axis=0)

    # split into train and test set
    is_test = np.random.rand(z.shape[0]) < 0.2
    z_train, z_test = z[np.logical_not(is_test)], z[is_test]
    assert z_train.shape[0] + z_test.shape[0] == z.shape[0]

    logger.info('z_train {} z_test {}'.format(z_train.shape, z_test.shape))

    logger.info('creating training data - done ')

    # --- train latent model

    # exponential moving averages: loss, sigma^2
    avg_train_loss, avg_train_sigma_sq = None, None
    avg_test_loss, avg_test_sigma_sq = None, None
    q_ema, best_avg_loss = 0.1, 9999

    batch_size = 128
    z_shape = [batch_size, z_train.shape[1], z_train.shape[2], z_train.shape[3]]

    # TODO make z_shape required attribute?
    model = models[model_type].create_new_model(z_shape=z_shape)

    train_generator = generate_batches_stackedae(z_train, batch_size)
    test_generator = generate_batches_stackedae(z_test, batch_size)

    if model.pretrain:

        logger.info('Pretraining encoder ...')

        max_pretrain_steps = 200

        for step in range(max_pretrain_steps):
            loss = model.pretrain_step(next(train_generator))

            # TODO get average?
            if step % 100 == 0:
                logger.info('loss {}'.format(loss))

            if loss < 0.1:
                break

        logger.info('Pretraining encoder - done')

    training_steps = int(1e6)
    print_std = False

    for step in range(training_steps):

        # --- train step

        train_loss, _, _, _ = model.train_step(next(train_generator))

        # --- compute test loss

        if step % 10 == 0:
            test_loss, _, _, _ = model.test_step(next(test_generator))

            avg_train_loss, avg_train_sigma_sq = update_ema_loss(avg_train_loss, avg_train_sigma_sq, train_loss, q_ema)
            avg_test_loss, avg_test_sigma_sq = update_ema_loss(avg_test_loss, avg_test_sigma_sq, test_loss, q_ema)

        if step % 100 == 0:
            if print_std:
                train_std, test_std = np.sqrt(avg_train_sigma_sq), np.sqrt(avg_test_sigma_sq)
                logger.info("""Step {} train loss {:1.3} +- {:1.3} test loss {:1.3} +- {:1.3}"""
                            .format(step, avg_train_loss, train_std, avg_test_loss, test_std))
            else:
                logger.info("""Step {} train loss {:1.3} test loss {:1.3}"""
                            .format(step, avg_train_loss, avg_test_loss))

        # --- save model

        if step % 1000 == 0 and avg_test_loss < best_avg_loss * 0.99:
            # save the new best model
            best_avg_loss = avg_test_loss
            model.save(stackedae_path)


if __name__ == '__main__':
    train_stacked_autoencoder()
