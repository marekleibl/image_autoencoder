"""
Visualize the interpolated shapes.
"""

from os.path import join
from matplotlib import pyplot as plt, animation
import numpy as np
import logging

import setting
from model.combined_ae import CombinedAE
from utils.utils import generate_batches

plt.style.use('dark_background')

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO, format='%(asctime)s:%(levelname)s:%(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')


def main():
    combinedAE = CombinedAE.load_model(setting.convAE_dir, setting.stackedAE_dir, 'WAE')
    batch_gen = generate_batches(**setting.cnn_hparams)

    z_prev = None
    z_next = combinedAE.encode(next(batch_gen))
    z_next = z_next[1] if type(z_next) == tuple else z_next

    fig = plt.figure()
    ax1 = fig.add_subplot(1, 1, 1)

    steps = 20

    def animate(i):
        nonlocal z_prev, z_next

        if i % steps == 0:
            batch = next(batch_gen)[:1]
            z_prev = z_next
            z_next = combinedAE.encode(batch)
            z_next = z_next[1] if type(z_next) == tuple else z_next

        t = (i % steps) / steps
        z_interp = (1 - t) * z_prev + t * z_next
        x_interp = combinedAE.decode(z_interp)
        im = x_interp[0, :, :, 0]

        ax1.clear()
        ax1.imshow(1 - im, cmap='gray')

    ani = animation.FuncAnimation(fig, animate, interval=100)
    plt.show()


if __name__ == '__main__':
    main()
