"""
Train a stacked autoencoder.
"""

from os.path import join
import numpy as np
import logging

import setting
from model.conv_ae import ConvAE
from model.models_dict import models
from utils.utils import generate_batches, update_ema_loss, deterministic_train_test_split

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO, format='%(asctime)s:%(levelname)s:%(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')


def get_latent_vectors(model, batch_gen, training_data_size):
    for i in range(training_data_size // setting.cnn_hparams['batch_size']):

        if i % 100 == 0:
            logger.info('{}/{}'.format(i, training_data_size))

        z = model.encode(next(batch_gen))
        yield z


def generate_batches_stackedae(z_data, batch_size):
    indexes = np.arange(z_data.shape[0])

    while True:
        ixs = np.random.choice(indexes, batch_size)
        yield z_data[ixs]


def train_stacked_autoencoder():
    # model_type = 'vanilla'
    model_type = 'VAE'
    # model_type = 'WAE'

    hparams = setting.cnn_hparams
    convae_path = setting.convAE_dir
    stackedae_path = join(setting.stackedAE_dir, model_type)

    # generate more if overfitting
    training_data_size = int(10 ** 4)

    # --- get model

    model = ConvAE.load_model(convae_path, **hparams)
    batch_gen = generate_batches(**setting.cnn_hparams)

    # --- create training data for latent autoencoder

    logger.info('creating training data ... ')

    z = get_latent_vectors(model, batch_gen, training_data_size)
    z = np.concatenate(tuple(z), axis=0)

    # split into train and test set
    z_train, z_test = deterministic_train_test_split(z)

    # is_test = np.random.rand(z.shape[0]) < 0.2
    # z_train, z_test = z[np.logical_not(is_test)], z[is_test]
    # assert z_train.shape[0] + z_test.shape[0] == z.shape[0]

    logger.info('z_train {} z_test {}'.format(z_train.shape, z_test.shape))

    logger.info('creating training data - done ')

    # --- train latent model

    # exponential moving averages: loss, sigma^2
    avg_train_loss, avg_train_sigma_sq = None, None
    avg_test_loss, avg_test_sigma_sq = None, None
    q_ema, best_avg_loss = 0.1, 9999

    batch_size = 128
    z_shape = [batch_size, z_train.shape[1], z_train.shape[2], z_train.shape[3]]

    # TODO make z_shape required attribute?
    model = models[model_type].create_new_model(z_shape=z_shape)

    train_generator = generate_batches_stackedae(z_train, batch_size)
    test_generator = generate_batches_stackedae(z_test, batch_size)

    if model.pretrain:

        logger.info('Pretraining encoder ...')

        max_pretrain_steps = 200

        for step in range(max_pretrain_steps):
            loss = model.pretrain_step(next(train_generator))

            # TODO get average?
            if step % 100 == 0:
                logger.info('loss {}'.format(loss))

            if loss < 0.1:
                break

        logger.info('Pretraining encoder - done')

    training_steps = int(1e6)

    train_losses, test_losses = [], []

    for step in range(training_steps):

        # --- train step

        total_loss, rec_loss, reg_loss, zreg_loss = model.train_step(next(train_generator))
        train_losses.append([total_loss, rec_loss, reg_loss, zreg_loss])

        # --- compute test loss

        if step % 10 == 0:
            total_loss, rec_loss, reg_loss, zreg_loss = model.test_step(next(test_generator))
            test_losses.append([total_loss, rec_loss, reg_loss, zreg_loss])

        if step % 500 == 0:
            # total loss is a sum of reconstuction loss + weights regularization + (optional) z regularization

            total_loss, rec_loss, reg_loss, zreg_loss = np.mean(train_losses, axis=0)
            logger.info("""Step {} | train loss total {:1.3} rec {:1.3} reg {:1.3} zreg {:1.3}""" \
                        .format(step, total_loss, rec_loss, reg_loss, zreg_loss))

            total_loss, rec_loss, reg_loss, zreg_loss = np.mean(test_losses, axis=0)
            logger.info("""Step {} | test  loss total {:1.3} rec {:1.3} reg {:1.3} zreg {:1.3}""" \
                        .format(step, total_loss, rec_loss, reg_loss, zreg_loss))

            avg_test_loss = total_loss
            train_losses, test_losses = [], []

        # --- save model

        if step % 1000 == 0 and avg_test_loss < best_avg_loss * 0.99:
            # save the new best model
            best_avg_loss = avg_test_loss
            model.save(stackedae_path)


if __name__ == '__main__':
    train_stacked_autoencoder()
