"""
Visualize the reconstructed shapes.
"""

from matplotlib import pyplot as plt
import numpy as np
import logging

import setting
from model.combined_ae import CombinedAE
from utils.utils import generate_batches

plt.style.use('dark_background')

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO, format='%(asctime)s:%(levelname)s:%(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')


def get_random_samples(batch_gen, nsamples):
    # generate random samples
    nbatches = np.ceil(nsamples / setting.cnn_hparams['batch_size']).astype(np.int32)
    batches = [next(batch_gen) for _ in range(nbatches)]
    return batches


def get_reconstuctions(batches, model_type):
    """
    Get random samples and reconstuctions using the given model.
    """
    # load model
    combinedAE = CombinedAE.load_model(setting.convAE_dir, setting.stackedAE_dir, model_type)

    # compute reconstuctions
    rec_batches = [combinedAE.reconstruct(batch) for batch in batches]

    # return arrays with shape [sample, image x, image y, features]
    batches, rec_batches = np.concatenate(batches, axis=0), np.concatenate(rec_batches, axis=0)
    return batches, rec_batches


def get_images(batches, model_type):
    x, x_rec = get_reconstuctions(batches, model_type)
    # shape [samples, image x, image y]
    im = np.concatenate((x[:, :, :, 0], x_rec[:, :, :, 0]), axis=2)
    return im


def main():
    batch_gen = generate_batches(**setting.cnn_hparams)

    nsamples = 20
    batches = get_random_samples(batch_gen, nsamples)

    im_ae = get_images(batches, 'vanilla')
    im_vae = get_images(batches, 'VAE')
    im_wae = get_images(batches, 'WAE')

    im = np.concatenate([im_ae, im_vae, im_wae], axis=1)

    for i in range(20):
        plt.imshow(1 - im[i], cmap='gray')
        plt.show()


if __name__ == '__main__':
    main()
