import logging
import numpy as np

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO, format='%(asctime)s:%(levelname)s:%(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
logger.setLevel(logging.DEBUG)

import setting
from model.conv_ae import ConvAE
from utils.utils import generate_batches, update_ema_loss


def train_conv_autoencoder():
    """
    Train convolutional autoencoder, i.e. AE that encodes images into 2D grids (low resolution image) of latent vectors.
    """

    hparams = setting.cnn_hparams
    model_path = setting.convAE_dir

    # create model
    model = ConvAE.create_new_model(**hparams)

    # training batches generator
    gen = generate_batches(**hparams)

    # exponential moving averages: loss, sigma^2
    avg_loss, avg_loss_sigma_sq = None, None
    q_ema, best_avg_loss = 0.01, 9999

    for step, batch in enumerate(gen):

        # --- train step

        loss, _, _, _ = model.train_step(batch)
        avg_loss, avg_loss_sigma_sq = update_ema_loss(avg_loss, avg_loss_sigma_sq, loss, q_ema)

        # --- print error

        if step % 100 == 0:
            logger.info('Step {} | loss {:1.3} +- {:1.3}'.format(step, avg_loss, np.sqrt(avg_loss_sigma_sq)))

        # --- save model

        if step % 1000 == 0 and step > 1000 and avg_loss < best_avg_loss * 0.99:
            # save the new best model
            best_avg_loss = avg_loss
            model.save(model_path)


if __name__ == '__main__':
    train_conv_autoencoder()
