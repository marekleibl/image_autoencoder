import logging
from os.path import join

import tensorflow as tf

import setting
from model.conv_ae import ConvAE
from model.models_dict import models

logger = logging.getLogger(__name__)


class CombinedAE:
    """
    Only for inference with trained model.

    Combined convolutional and stacked autoencoder.
    """

    def __init__(self, convae, stackedae):
        self._convae = convae
        self._stackedae = stackedae

    @tf.function
    def encode(self, x):
        z = self._stackedae.encode(self._convae.encode(x))
        return z

    # @tf.function
    def decode(self, z):
        if type(z) == tuple:
            z = z[0]

        x_rec = self._convae.decode(self._stackedae.decode(z))
        return x_rec

    # @tf.function
    def reconstruct(self, x):
        z = self.encode(x)
        return self.decode(z)

    @classmethod
    def load_model(cls, convAE_dir, stackedAE_dir, model_type):
        logger.info('Loading convae from ' + convAE_dir)
        convae = ConvAE.load_model(convAE_dir)

        logger.info('Loading stackedae from ' + stackedAE_dir)
        stackedae = models[model_type].load_model(join(setting.stackedAE_dir, model_type))

        logger.info('Loading models - done ')
        return CombinedAE(convae, stackedae)
