# i Copyright 2017 Max Planck Society
# Distributed under the BSD-3 Software license,
# (See accompanying file ./LICENSE.txt or copy at
# https://opensource.org/licenses/BSD-3-Clause)

""" Wasserstein Auto-Encoder models

"""

import sys
import time
import os
import numpy as np
import tensorflow as tf
import logging


class WAE(object):

    def __init__(self, opts, train_size=0):

        logging.error('Building the Tensorflow Graph')

        # -- Placeholders

        self.add_inputs_placeholders()
        self.add_training_placeholders()
        sample_size = tf.shape(self.sample_points)[0]

        # -- Transformation ops

        # Encode the content of sample_points placeholder
        res = encoder(opts, inputs=self.sample_points, is_training=self.is_training)

        if opts['e_noise'] in ('deterministic', 'implicit', 'add_noise'):
            self.enc_mean, self.enc_sigmas = None, None
            if opts['e_noise'] == 'implicit':
                self.encoded, self.encoder_A = res
            else:
                self.encoded, _ = res
        elif opts['e_noise'] == 'gaussian':
            # Encoder outputs means and variances of Gaussian
            enc_mean, enc_sigmas = res[0]
            enc_sigmas = tf.clip_by_value(enc_sigmas, -50, 50)
            self.enc_mean, self.enc_sigmas = enc_mean, enc_sigmas
            if opts['verbose']:
                self.add_sigmas_debug()

            eps = tf.random_normal((sample_size, opts['zdim']), 0., 1., dtype=tf.float32)
            self.encoded = self.enc_mean + tf.multiply(eps, tf.sqrt(1e-8 + tf.exp(self.enc_sigmas)))
            # self.encoded = self.enc_mean + tf.multiply(eps, tf.exp(self.enc_sigmas / 2.))

        # Decode the points encoded above (i.e. reconstruct)
        self.reconstructed, self.reconstructed_logits = \
            decoder(opts, noise=self.encoded, is_training=self.is_training)

        # Decode the content of sample_noise
        self.decoded, self.decoded_logits = \
            decoder(opts, reuse=True, noise=self.sample_noise, is_training=self.is_training)

        # -- Objectives, losses, penalties

        self.penalty, self.loss_gan = self.matching_penalty()
        self.loss_reconstruct = self.reconstruction_loss(
            self.opts, self.sample_points, self.reconstructed)
        self.wae_objective = self.loss_reconstruct + \
                             self.wae_lambda * self.penalty

        # Extra costs if any
        if 'w_aef' in opts and opts['w_aef'] > 0:
            improved_wae.add_aefixedpoint_cost(opts, self)

        self.blurriness = self.compute_blurriness()

        if opts['e_pretrain']:
            self.loss_pretrain = self.pretrain_loss()
        else:
            self.loss_pretrain = None

        self.add_least_gaussian2d_ops()

        # -- Optimizers, savers, etc

        self.add_optimizers()
        self.add_savers()
        self.init = tf.global_variables_initializer()

    def add_inputs_placeholders(self):
        opts = self.opts
        shape = self.data_shape
        data = tf.placeholder(
            tf.float32, [None] + shape, name='real_points_ph')
        noise = tf.placeholder(
            tf.float32, [None] + [opts['zdim']], name='noise_ph')

        self.sample_points = data
        self.sample_noise = noise

    def add_training_placeholders(self):
        opts = self.opts
        decay = tf.placeholder(tf.float32, name='rate_decay_ph')
        wae_lambda = tf.placeholder(tf.float32, name='lambda_ph')
        is_training = tf.placeholder(tf.bool, name='is_training_ph')
        self.lr_decay = decay
        self.wae_lambda = wae_lambda
        self.is_training = is_training

    def pretrain_loss(self):
        opts = self.opts
        # Adding ops to pretrain the encoder so that mean and covariance
        # of Qz will try to match those of Pz
        mean_pz = tf.reduce_mean(self.sample_noise, axis=0, keep_dims=True)
        mean_qz = tf.reduce_mean(self.encoded, axis=0, keep_dims=True)
        mean_loss = tf.reduce_mean(tf.square(mean_pz - mean_qz))
        cov_pz = tf.matmul(self.sample_noise - mean_pz,
                           self.sample_noise - mean_pz, transpose_a=True)
        cov_pz /= opts['e_pretrain_sample_size'] - 1.
        cov_qz = tf.matmul(self.encoded - mean_qz,
                           self.encoded - mean_qz, transpose_a=True)
        cov_qz /= opts['e_pretrain_sample_size'] - 1.
        cov_loss = tf.reduce_mean(tf.square(cov_pz - cov_qz))
        return mean_loss + cov_loss

    def matching_penalty(self):
        opts = self.opts
        loss_gan = None
        sample_qz = self.encoded
        sample_pz = self.sample_noise
        if opts['z_test'] == 'gan':
            loss_gan, loss_match = self.gan_penalty(sample_qz, sample_pz)
        elif opts['z_test'] == 'mmd':
            loss_match = self.mmd_penalty(sample_qz, sample_pz)
        elif opts['z_test'] == 'mmdpp':
            loss_match = improved_wae.mmdpp_penalty(
                opts, self, sample_pz)
        elif opts['z_test'] == 'mmdppp':
            loss_match = improved_wae.mmdpp_1d_penalty(
                opts, self, sample_pz)
        else:
            assert False, 'Unknown penalty %s' % opts['z_test']
        return loss_match, loss_gan

    def mmd_penalty(self, sample_qz, sample_pz):
        opts = self.opts
        sigma2_p = opts['pz_scale'] ** 2
        kernel = opts['mmd_kernel']
        n = utils.get_batch_size(sample_qz)
        n = tf.cast(n, tf.int32)
        nf = tf.cast(n, tf.float32)
        half_size = (n * n - n) / 2

        norms_pz = tf.reduce_sum(tf.square(sample_pz), axis=1, keep_dims=True)
        dotprods_pz = tf.matmul(sample_pz, sample_pz, transpose_b=True)
        distances_pz = norms_pz + tf.transpose(norms_pz) - 2. * dotprods_pz

        norms_qz = tf.reduce_sum(tf.square(sample_qz), axis=1, keep_dims=True)
        dotprods_qz = tf.matmul(sample_qz, sample_qz, transpose_b=True)
        distances_qz = norms_qz + tf.transpose(norms_qz) - 2. * dotprods_qz

        dotprods = tf.matmul(sample_qz, sample_pz, transpose_b=True)
        distances = norms_qz + tf.transpose(norms_pz) - 2. * dotprods

        # if opts['verbose']:
        #     distances = tf.Print(
        #         distances,
        #         [tf.nn.top_k(tf.reshape(distances_qz, [-1]), 1).values[0]],
        #         'Maximal Qz squared pairwise distance:')
        #     distances = tf.Print(distances, [tf.reduce_mean(distances_qz)],
        #                         'Average Qz squared pairwise distance:')

        #     distances = tf.Print(
        #         distances,
        #         [tf.nn.top_k(tf.reshape(distances_pz, [-1]), 1).values[0]],
        #         'Maximal Pz squared pairwise distance:')
        #     distances = tf.Print(distances, [tf.reduce_mean(distances_pz)],
        #                         'Average Pz squared pairwise distance:')

        if kernel == 'RBF':
            # Median heuristic for the sigma^2 of Gaussian kernel
            sigma2_k = tf.nn.top_k(
                tf.reshape(distances, [-1]), half_size).values[half_size - 1]
            sigma2_k += tf.nn.top_k(
                tf.reshape(distances_qz, [-1]), half_size).values[half_size - 1]
            # Maximal heuristic for the sigma^2 of Gaussian kernel
            # sigma2_k = tf.nn.top_k(tf.reshape(distances_qz, [-1]), 1).values[0]
            # sigma2_k += tf.nn.top_k(tf.reshape(distances, [-1]), 1).values[0]
            # sigma2_k = opts['latent_space_dim'] * sigma2_p
            if opts['verbose']:
                sigma2_k = tf.Print(sigma2_k, [sigma2_k], 'Kernel width:')
            res1 = tf.exp(- distances_qz / 2. / sigma2_k)
            res1 += tf.exp(- distances_pz / 2. / sigma2_k)
            res1 = tf.multiply(res1, 1. - tf.eye(n))
            res1 = tf.reduce_sum(res1) / (nf * nf - nf)
            res2 = tf.exp(- distances / 2. / sigma2_k)
            res2 = tf.reduce_sum(res2) * 2. / (nf * nf)
            stat = res1 - res2
        elif kernel == 'IMQ':
            # k(x, y) = C / (C + ||x - y||^2)
            # C = tf.nn.top_k(tf.reshape(distances, [-1]), half_size).values[half_size - 1]
            # C += tf.nn.top_k(tf.reshape(distances_qz, [-1]), half_size).values[half_size - 1]
            if opts['pz'] == 'normal':
                Cbase = 2. * opts['zdim'] * sigma2_p
            elif opts['pz'] == 'sphere':
                Cbase = 2.
            elif opts['pz'] == 'uniform':
                # E ||x - y||^2 = E[sum (xi - yi)^2]
                #               = zdim E[(xi - yi)^2]
                #               = const * zdim
                Cbase = opts['zdim']
            stat = 0.
            for scale in [.1, .2, .5, 1., 2., 5., 10.]:
                C = Cbase * scale
                res1 = C / (C + distances_qz)
                res1 += C / (C + distances_pz)
                res1 = tf.multiply(res1, 1. - tf.eye(n))
                res1 = tf.reduce_sum(res1) / (nf * nf - nf)
                res2 = C / (C + distances)
                res2 = tf.reduce_sum(res2) * 2. / (nf * nf)
                stat += res1 - res2
        return stat

    def sample_pz(self, num=100):
        opts = self.opts
        mean = np.zeros(opts["zdim"])
        cov = np.identity(opts["zdim"])
        noise = np.random.multivariate_normal(mean, cov, num).astype(np.float32)
        return opts['pz_scale'] * noise

    def pretrain_encoder(self, data):
        opts = self.opts
        steps_max = 200
        batch_size = opts['e_pretrain_sample_size']

        for step in range(steps_max):
            train_size = data.num_points
            data_ids = np.random.choice(train_size, min(train_size, batch_size),
                                        replace=False)
            batch_images = data.data[data_ids].astype(np.float)
            batch_noise = self.sample_pz(batch_size)

            [_, loss_pretrain] = self.sess.run([self.pretrain_opt, self.loss_pretrain],
                                               feed_dict={self.sample_points: batch_images,
                                                          self.sample_noise: batch_noise,
                                                          self.is_training: True})

            if opts['verbose']:
                logging.error('Step %d/%d, loss=%f' % (step, steps_max, loss_pretrain))

            if loss_pretrain < 0.1:
                break

    def train(self, data):
        opts = self.opts
        if opts['verbose']:
            logging.error(opts)
        logging.error('Training WAE')
        losses = []
        losses_rec = []
        losses_match = []
        batches_num = data.num_points / opts['batch_size']
        train_size = data.num_points
        self.num_pics = opts['plot_num_pics']
        self.fixed_noise = self.sample_pz(opts['plot_num_pics'])

        self.sess.run(self.init)

        if opts['e_pretrain']:
            logging.error('Pretraining the encoder')
            self.pretrain_encoder(data)
            logging.error('Pretraining the encoder done.')

        self.start_time = time.time()
        counter = 0
        decay = 1.
        wae_lambda = opts['lambda']

        for epoch in range(opts["epoch_num"]):

            # Save the model

            if epoch > 0 and epoch % opts['save_every_epoch'] == 0:
                self.saver.save(self.sess,
                                os.path.join(opts['work_dir'],
                                             'checkpoints',
                                             'trained-wae'),
                                global_step=counter)

            # Iterate over batches

            for it in range(batches_num):

                # Sample batches of data points and Pz noise

                data_ids = np.random.choice(
                    train_size, opts['batch_size'], replace=False)
                batch_images = data.data[data_ids].astype(np.float)
                batch_noise = self.sample_pz(opts['batch_size'])

                # Update encoder and decoder

                feed_d = {
                    self.sample_points: batch_images,
                    self.sample_noise: batch_noise,
                    self.lr_decay: decay,
                    self.wae_lambda: wae_lambda,
                    self.is_training: True}

                [_, loss, loss_rec, loss_match] = self.sess.run(
                    [self.ae_opt,
                     self.wae_objective,
                     self.loss_reconstruct,
                     self.penalty],
                    feed_dict=feed_d)

                # Update learning rate if necessary
                losses.append(loss)
                losses_rec.append(loss_rec)
                losses_match.append(loss_match)
                if opts['verbose']:
                    logging.error('Matching penalty after %d steps: %f' % (counter, losses_match[-1]))

                counter += 1
