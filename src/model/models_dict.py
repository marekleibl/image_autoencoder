from model.stacked_ae import StackedAE
from model.stacked_vae import StackedVAE
from model.stacked_wae import StackedWAE

models = {
    'vanilla': StackedAE,
    'VAE': StackedVAE,
    'WAE': StackedWAE
}
