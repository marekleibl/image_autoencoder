import tensorflow as tf
from tensorflow.keras import regularizers
from tensorflow.keras.layers import Dense, Flatten, Reshape

from model.autoencoder import Autoencoder


class StackedAE(Autoencoder):
    """
    Stacked autoencoder.
    Encodes the latent vectors/images of the original larger AE into fewer latent parameters.

    Note that we avoid confusion we use:
      * 'z' for the original latent vectors/images and
      * 'zz' for latent vectors of stacked AE.
    """

    def _build_model(self, **kwargs):
        z_shape = kwargs.get('z_shape')
        nfeatures = 512
        zz_dim = 10
        l2_alpha = 1e-5
        learning_rate = 1e-4

        dense_params = {
            'activation': tf.nn.leaky_relu,
            'kernel_initializer': tf.initializers.glorot_normal(),
            'kernel_regularizer': regularizers.l2(l2_alpha)
        }

        out_features = z_shape[1] * z_shape[2] * z_shape[3]

        size = 2 * zz_dim if self.add_logvar else zz_dim

        self._encoder = tf.keras.Sequential([
            Flatten(),
            Dense(nfeatures, **dense_params),
            Dense(size, **dense_params)
        ])

        self._decoder = tf.keras.Sequential([
            Dense(nfeatures, **dense_params),
            Dense(out_features, **dense_params),
            Reshape(z_shape[1:])
        ])

        self._optimizer = tf.keras.optimizers.Adam(learning_rate)
        self._loss = tf.keras.losses.MeanSquaredError()
