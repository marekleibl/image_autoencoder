import tensorflow as tf
from tensorflow.keras import regularizers
from tensorflow.keras.layers import Dense, Flatten, Reshape
from tensorflow_core.python.keras.losses import MeanSquaredError

from model.autoencoder import Autoencoder
import numpy as np

from model.stacked_ae import StackedAE


class StackedVAE(StackedAE):
    """
    Variational autoencoder.

    Based on:
    https://www.tensorflow.org/tutorials/generative/cvae
    https://www.tensorflow.org/guide/keras/custom_layers_and_models#putting_it_all_together_an_end-to-end_example
    """

    def __init__(self):
        super(StackedVAE, self).__init__()
        self.add_logvar = True

    def _compute_loss(self, x):
        # --- encode & decode

        z, mean, logvar = self.encode(x)
        x_rec = self.decode(z)

        rec_loss = MeanSquaredError()(x, x_rec)

        # --- VAE loss

        # KL divergence regularization loss
        # see this for more details how we get this formula:
        # https://towardsdatascience.com/variational-autoencoders-63191b75c576
        zreg_loss = -0.5 * tf.reduce_mean(logvar - tf.square(mean) - tf.exp(logvar) + 1)

        # --- regularization

        # add regularization term (L2 regularization)
        reg_loss = tf.math.add_n(self._encoder.losses) + tf.math.add_n(self._decoder.losses)
        total_loss = rec_loss + reg_loss + zreg_loss

        return total_loss, rec_loss, reg_loss, zreg_loss

    @tf.function
    def encode(self, x):
        """
        Unlike vanilla autoencoder, encoding is stochastic.

        :return a tuple (a sample from N(mean, var), mean, var)
        """
        # compute mean and logvar
        encoded = self._encoder(x)
        mean, logvar = tf.split(encoded, num_or_size_splits=2, axis=1)

        # reparametrize
        eps = tf.random.normal(shape=mean.shape)
        z = eps * tf.exp(logvar * .5) + mean

        return z, mean, logvar

    @tf.function
    def decode(self, z, *args):
        return self._decoder(z)
