import os
from os.path import join, abspath

project_dir = abspath(join(__file__, os.pardir, os.pardir))
print(project_dir)

data_dir = join(project_dir, 'data')
model_dir = join(data_dir, 'models')
convAE_dir = join(model_dir, 'convAE')
stackedAE_dir = join(model_dir, 'stackedAE')

# TODO separate for both AEs?
# model hyper parameters
cnn_hparams = {
    'image_shape': [16, 16],
    'learning_rate': 1e-4,
    'batch_size': 128,  # 128,
    'l2_alpha': 1e-6,
    'nbasefilters': 64,  # 32,
    'zdim': 20,
    'ntraining_steps': int(10 ** 5)
}

