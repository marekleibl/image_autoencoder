import numpy as np
import logging

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO, format='%(asctime)s:%(levelname)s:%(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
logger.setLevel(logging.DEBUG)

from utils.data_generator import random_circle


def generate_batches(batch_size, **kwargs):
    """
    Return a generator of batches with randomly generated circles.
    """
    return_tuple = kwargs.get('return_tuple')

    # estimate mu, sigma on a sample
    sample_size = 200
    sample = np.stack([random_circle() for _ in range(sample_size)], axis=0)
    mu, sigma = np.mean(sample), np.std(sample)
    assert sigma > 0

    while True:
        # generate the next batch
        batch = np.stack([random_circle() for _ in range(batch_size)], axis=0)

        # normalize
        batch = (batch[:, :, :, np.newaxis] - mu) / sigma

        if return_tuple:
            yield batch, batch
        else:
            yield batch


def update_ema(avg_loss, new_loss, q):
    """
    Update exponential moving average.
    """
    if avg_loss is None:
        return new_loss

    return (1 - q) * avg_loss + q * new_loss


def update_ema_loss(avg_loss, avg_loss_sigma_sq, new_loss, q):
    """
    Update ema loss and ema estimate of variance.
    """
    avg_loss = update_ema(avg_loss, new_loss, q)
    avg_loss_sigma_sq = update_ema(avg_loss_sigma_sq, (new_loss - avg_loss) ** 2, q)
    return avg_loss, avg_loss_sigma_sq


def deterministic_train_test_split(xx, nfolds=5):
    """
    Split data into train / test set deterministically along zero axis.
    """
    is_test = np.array([hash(i) % nfolds == 0 for i in range(xx.shape[0])])
    xx_train, xx_test = xx[np.logical_not(is_test)], xx[is_test]
    assert xx_train.shape[0] + xx_test.shape[0] == xx.shape[0]
    assert xx_train.shape[0] > 0 and xx_test.shape[0] > 0
    return xx_train, xx_test
