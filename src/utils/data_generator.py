import numpy as np
from numpy.random import rand


def random_circle(im_shape=[16, 16]):
    """
    Generate an image with a circle of random position, radius and thickness.
    """

    xsize, ysize = im_shape

    # --- generate random params

    # center of the circle
    sx = xsize / 2 + (xsize / 4) * (2 * rand() - 1)
    sy = ysize / 2 + (ysize / 4) * (2 * rand() - 1)

    # radius
    msize = min(xsize, ysize)
    r = (msize / 4) + (msize / 4) * rand()

    # relative thickness of the circle
    b = 0.1 + 0.4 * rand()
    assert 0 < b < 1

    # --- create circle image

    # 2d grid
    xx, yy = np.meshgrid(np.arange(xsize), np.arange(ysize))

    # positive inside the circle, negative outside
    dd = (xx - sx) ** 2 + (yy - sy) ** 2 - r ** 2
    dd2 = (xx - sx) ** 2 + (yy - sy) ** 2 - (r * (1 - b)) ** 2

    # "soft" indicator of whether the pixel is inside innet/outer circle
    inside_outer = 1 / (1 + np.exp(-dd))
    inside_inner = 1 / (1 + np.exp(-dd2))

    # the final image: close to 1 between inner and outer circle, otherwise close to 0
    im = inside_outer - inside_inner

    return im.astype(np.float32)


def plot_circles():
    from matplotlib import pyplot as plt
    plt.style.use('dark_background')

    while True:
        circle = random_circle()

        plt.imshow(circle, cmap='gray')
        plt.show()


if __name__ == '__main__':
    plot_circles()
