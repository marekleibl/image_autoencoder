import numpy as np
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()


def _conv2d_trans_weights(in_size, out_size, kernel_size=4, stride=2):
    """
    Initialize with bilinear interpolations (for kernel size 4 and stride 2).

    Based on this code https://github.com/MarvinTeichmann/tensorflow-fcn/blob/master/fcn8_vgg.py
    """
    # Xavier initialization
    n_inputs = in_size * (kernel_size / stride) ** 2
    stddev = np.sqrt(2 / n_inputs)

    # bilinear filter
    f_bilinear = np.ones([kernel_size, kernel_size, 1, 1], dtype=np.float32)

    # random combinations between i-th and j-th channel
    initial_value = tf.truncated_normal([1, 1, out_size, in_size], stddev=stddev)
    w = tf.Variable(initial_value)

    # shape [height, width, output_channels, in_channels]
    W = w * f_bilinear
    return W


def deconv(x, in_size, out_size, shape):
    w = _conv2d_trans_weights(in_size, out_size)
    deconv = tf.nn.conv2d_transpose(x, w, shape, strides=[1, 2, 2, 1])
    return deconv
