"""

"""

# TODO finish this
# TODO test it with tf 1  (e.q. 1.14)

from functools import partial

import tensorflow.compat.v1 as tf
from tensorflow.compat.v1.layers import max_pooling2d as max_pool

# from tensorflow.keras import layers

tf.disable_v2_behavior()

# --- public
from bak.tf1.tf_utils import deconv


class AEModelTF1:
    def __init__(self, **kwargs):
        self._build_model(**kwargs)

        self._sess = tf.Session()
        self._sess.run(tf.global_variables_initializer())

    def train_step(self, x):
        sess, model = self._sess, self._model

        _, loss = sess.run([model['train_op'], model['loss']], feed_dict={model['x']: x})
        return loss

    @staticmethod
    def _xavier_init():
        # tf 1.x
        # return tf.contrib.layers.xavier_initializer_conv2d()
        return tf.initializers.glorot_normal()


    def _build_model(self, **kwargs):
        # TODO flexible batch size

        # --- parameters

        image_shape = kwargs.get('image_shape')
        learning_rate = kwargs.get('learning_rate')
        batch_size = kwargs.get('batch_size')
        l2_alpha = kwargs.get('l2_alpha')
        nf = kwargs.get('nbasefilters', 32)
        z_dim = kwargs.get('zdim', 10)
        input_features = kwargs.get('input_features', 1)

        x_shape = [batch_size, image_shape[0], image_shape[1], input_features]

        # ---  encoder

        # input image
        x = tf.placeholder(tf.float32, x_shape, name='x_image')

        conv2d = partial(tf.layers.conv2d, padding='same', activation=tf.nn.leaky_relu,
                         kernel_initializer=self._xavier_init(), kernel_size=3)
        # dense = partial(tf.layers.dense, activation=tf.nn.leaky_relu,
        #                 kernel_initializer=tf.contrib.layers.xavier_initializer())

        conv1 = conv2d(x, filters=nf)
        conv2 = conv2d(conv1, filters=2 * nf)

        pool1 = max_pool(inputs=conv2, pool_size=[2, 2], strides=2)

        conv2 = conv2d(pool1, filters=2 * nf)
        conv3 = conv2d(conv2, filters=4 * nf)

        pool4 = max_pool(inputs=conv3, pool_size=[2, 2], strides=2)
        conv5 = conv2d(pool4, filters=4 * nf)
        conv6 = conv2d(conv5, filters=z_dim)

        z = conv6

        # --- decoder

        # apply convolutions
        conv7 = conv2d(conv6, filters=4 * nf)
        conv8 = conv2d(conv7, filters=4 * nf)

        # upconvolutions
        # deconv6 = _deconv(conv5, nf, input_features, x_shape)

        deconv9 = deconv(conv8, 4 * nf, 2 * nf, [x_shape[0], x_shape[1] // 2, x_shape[2] // 2, nf * 2])
        conv10 = conv2d(deconv9, 2 * nf)

        deconv11 = deconv(conv10, 2 * nf, nf, x_shape[:3] + [nf])
        conv12 = conv2d(deconv11, input_features)

        # reconstructed input
        x_rec = conv12

        # --- training

        base_loss = tf.reduce_mean((x_rec - x) ** 2, name="base_loss")

        l2_loss = tf.add_n([tf.reduce_sum(w ** 2) for w in tf.trainable_variables()])
        loss = base_loss + l2_alpha * l2_loss

        train_op = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(loss, name="fcn_train_op")

        saver = tf.train.Saver(max_to_keep=1)

        model = {
            'x': x,
            'x_rec': x_rec,
            'z': z,
            'train_op': train_op,
            'loss': loss,
            'saver': saver
        }

        self._model = model
