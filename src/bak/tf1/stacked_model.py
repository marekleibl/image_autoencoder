import tensorflow.compat.v1 as tf

tf.disable_v2_behavior()


def stacked_ae_model(z_shape, **kwargs):
    """
    Stacked AE.

    Autoencoding latent vector or images of the original larger AE.

    Note that we avoid confussion we use:
      * 'z' to denote original latent vectors and
      * 'zz' latent vectors of stacked AE.

    :param z_shape assuming shape [batch, x_size, y_size, z features]
    """

    nfeatures = 512
    zz_dim = 10
    l2_alpha = 1e-5
    learning_rate = 1e-4

    # latent vector of the original AE
    z = tf.placeholder(tf.float32, z_shape, name='x_image')

    def dense(x, features):
        """
        Fully connected layer.
        """
        # relu does not work well for autoencoders
        activation = tf.nn.leaky_relu
        init = tf.contrib.layers.xavier_initializer()

        d = tf.keras.layers.Dense(features, activation=activation, kernel_initializer=init)
        return d(x)

    # flatten images to feature vectors
    z_flatten = tf.reshape(z, [z_shape[0], -1])

    h1 = dense(z_flatten, nfeatures)
    h2 = dense(h1, nfeatures)
    h3 = dense(h2, zz_dim)
    zz = h3
    h4 = dense(h3, nfeatures)

    out_features = z_shape[1] * z_shape[2] * z_shape[3]
    z_rec = dense(h4, out_features)

    # reshape back to images
    z_rec = tf.reshape(z_rec, z_shape)

    # --- training

    base_loss = tf.reduce_mean((z_rec - z) ** 2, name="base_loss")

    l2_loss = tf.add_n([tf.reduce_sum(w ** 2) for w in tf.trainable_variables()])
    loss = base_loss + l2_alpha * l2_loss

    train_op = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(loss, name="fcn_train_op")

    saver = tf.train.Saver(max_to_keep=1)

    model = {
        'z': z,
        'z_rec': z_rec,
        'zz': zz,
        'train_op': train_op,
        'loss': loss,
        'saver': saver
    }

    return model
