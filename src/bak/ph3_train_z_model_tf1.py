"""
Train a stacked autoencoder.
"""

import os
from os.path import join

import numpy as np
import logging
import tensorflow as tf

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO, format='%(asctime)s:%(levelname)s:%(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')

import setting
from bak.tf1.ae_model import stacked_ae_model
from utils.utils import load_model
from utils.utils import generate_batches, update_ema_loss


def get_latent_vectors(model, batch_gen, training_data_size):
    for i in range(training_data_size // setting.hparams['batch_size']):

        if i % 100 == 0:
            logger.info('{}/{}'.format(i, training_data_size))

        # shape [batch, x, y, features]
        x = next(batch_gen)

        z = model['sess'].run(model['z'], feed_dict={model['x']: x})
        yield z


def generate_batches_stackedae(z_data, batch_size):
    indexes = np.arange(z_data.shape[0])

    while True:
        ixs = np.random.choice(indexes, batch_size)
        yield z_data[ixs]


def main():
    model_name = 'stackedAE'
    model_dir = setting.convAE_dir
    model_path = join(model_dir, model_name)

    # generate more if overfitting
    training_data_size = int(10 ** 4)

    # --- get tf1

    model = load_model()
    batch_gen = generate_batches(**setting.hparams)

    # --- create training data for latent autoencoder

    logger.info('creating training data ... ')

    z = get_latent_vectors(model, batch_gen, training_data_size)
    z = np.concatenate(tuple(z), axis=0)

    # split into train and test set
    is_test = np.random.rand(z.shape[0]) < 0.2
    z_train, z_test = z[np.logical_not(is_test)], z[is_test]
    assert z_train.shape[0] + z_test.shape[0] == z.shape[0]

    logger.info('z_train {} z_test {}'.format(z_train.shape, z_test.shape))

    logger.info('creating training data - done ')

    # --- train latent tf1

    # loss and sigma^2 (exp weighted average)
    avg_train_loss, avg_train_loss_sigma_sq = None, 0
    avg_test_loss, avg_test_loss_sigma_sq = None, 0
    q_ema = 0.01
    best_avg_loss = 9999

    batch_size = 128
    z_shape = [batch_size, z_train.shape[1], z_train.shape[2], z_train.shape[3]]

    model = stacked_ae_model(z_shape)

    train_generator = generate_batches_stackedae(z_train, batch_size)
    test_generator = generate_batches_stackedae(z_test, batch_size)

    training_stes = int(1e6)

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())

        for step in range(training_stes):

            # --- training step

            _, train_loss = sess.run([model['train_op'], model['loss']], feed_dict={model['z']: next(train_generator)})

            avg_train_loss, avg_train_loss_sigma_sq = \
                update_ema_loss(avg_train_loss, avg_train_loss_sigma_sq, train_loss, q_ema)

            # --- compute test loss

            if step % 10 == 0:
                test_loss = sess.run(model['loss'], feed_dict={model['z']: next(test_generator)})

                avg_test_loss, avg_test_loss_sigma_sq = \
                    update_ema_loss(avg_test_loss, avg_test_loss_sigma_sq, test_loss, q_ema * 10)

            if step % 100 == 0:
                logger.info("""Step {} train loss {:1.3} +- {:1.3} test loss {:1.3} +- {:1.3}"""
                            .format(step, avg_train_loss, np.sqrt(
                    avg_train_loss_sigma_sq), avg_test_loss, np.sqrt(avg_test_loss_sigma_sq)))

            # --- save tf1
            # note that we only save models with the new best loss

            if step % 500 == 0 and avg_test_loss < best_avg_loss * 0.99:
                best_avg_loss = avg_test_loss

                os.makedirs(model_dir, exist_ok=True)
                model['saver'].save(sess, model_path, global_step=step)
                logger.info('Model saved to ' + model_path)


if __name__ == '__main__':
    main()
