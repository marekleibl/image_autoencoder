"""

"""

# TODO an alternative would be Fully conv. AE + stacked AE


from functools import partial
import numpy as np

import tensorflow as tf
from tensorflow.layers import max_pooling2d as max_pool


def _conv2d_trans_weights(in_size, out_size, kernel_size=4, stride=2):
    """
    Initialize with bilinear interpolations (for kernel size 4 and stride 2).
    """
    # see this https://github.com/MarvinTeichmann/tensorflow-fcn/blob/master/fcn8_vgg.py

    # Xavier initialization
    n_inputs = in_size * (kernel_size / stride) ** 2
    stddev = np.sqrt(2 / n_inputs)

    # bilinear filter
    f_bilinear = np.ones([kernel_size, kernel_size, 1, 1], dtype=np.float32)

    # random combinations between i-th and j-th channel
    initial_value = tf.truncated_normal([1, 1, out_size, in_size], stddev=stddev)
    w = tf.Variable(initial_value)

    # shape [height, width, output_channels, in_channels]
    W = w * f_bilinear
    return W


def _deconv(x, in_size, out_size, shape):
    w = _conv2d_trans_weights(in_size, out_size)
    deconv = tf.nn.conv2d_transpose(x, w, shape, strides=[1, 2, 2, 1])
    return deconv


# --- public


def ae_model(**kwargs):
    # TODO flexible batch size

    # --- parameters

    image_shape = kwargs.get('image_shape')
    learning_rate = kwargs.get('learning_rate')
    batch_size = kwargs.get('batch_size')
    l2_alpha = kwargs.get('l2_alpha')
    nf = kwargs.get('nbasefilters', 32)
    z_dim = kwargs.get('zdim', 10)

    x_shape = [batch_size, image_shape[0], image_shape[1], 1]

    # ---  encoder

    # input image
    x = tf.placeholder(tf.float32, x_shape, name='x_image')

    conv2d = partial(tf.layers.conv2d, padding='same', activation=tf.nn.leaky_relu,
                     kernel_initializer=tf.contrib.layers.xavier_initializer_conv2d())
    dense = partial(tf.layers.dense, activation=tf.nn.leaky_relu,
                    kernel_initializer=tf.contrib.layers.xavier_initializer())

    conv1 = conv2d(x, filters=nf, kernel_size=3)
    pool1 = max_pool(inputs=conv1, pool_size=[2, 2], strides=2)

    conv2 = conv2d(pool1, filters=2 * nf, kernel_size=3)
    conv3 = conv2d(conv2, filters=2 * nf, kernel_size=3)

    # fully connected
    fcn4 = dense(conv3, 4 * nf)
    fcn5 = dense(fcn4, z_dim)
    z = fcn5

    # --- decoder

    # fully connected
    fcn6 = dense(fcn5, 4 * nf)

    # fully connected (output is image)
    fcn7 = dense(fcn6, conv3.shape[1] * conv3.shape[2] * conv3.shape[3])
    fcn7 = tf.reshape(fcn7, conv3.shape)

    # apply convolutions
    conv8 = conv2d(fcn7, filters=2 * nf, kernel_size=3)
    conv9 = conv2d(conv8, filters=nf, kernel_size=3)

    # upconvolutions
    deconv10 = _deconv(conv9, nf, nf, conv3.shape)
    x_rec = deconv10

    # --- training

    base_loss = tf.reduce_mean((x_rec - x) ** 2, name="base_loss")

    l2_loss = tf.add_n([tf.reduce_sum(w ** 2) for w in tf.trainable_variables()])
    loss = base_loss + l2_alpha * l2_loss

    train_op = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(loss, name="fcn_train_op")

    model = {
        'x': x,
        'x_rec': x_rec,
        'z': z,
        'train_op': train_op,
        'loss': loss
    }

    return model
