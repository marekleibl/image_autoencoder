"""
Visualize the interpolated shapes.
"""

from os.path import join
from matplotlib import pyplot as plt
import numpy as np
import logging

import setting
from model.combined_ae import CombinedAE
from utils.utils import generate_batches

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO, format='%(asctime)s:%(levelname)s:%(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')


def main():
    combinedAE = CombinedAE.load_model(setting.convAE_dir, setting.stackedAE_dir)
    batch_gen = generate_batches(**setting.hparams)

    z_next = combinedAE.encode(next(batch_gen))

    for i in range(10):

        z_prev = z_next
        z_next = combinedAE.encode(next(batch_gen))

        for t in np.linspace(0, 1, 10):
            z = (1 - t) * z_prev + t * z_next
            x_rec = combinedAE.decode(z)
            im = x_rec[i, :, :, 0]  

            plt.imshow(im, cmap='gray')
            plt.show()


if __name__ == '__main__':
    main()
