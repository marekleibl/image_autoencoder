"""
Train an AE tf1 on artificial data.
"""

import os
from os.path import join
import logging
import numpy as np

import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO, format='%(asctime)s:%(levelname)s:%(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
logger.setLevel(logging.DEBUG)

import setting
from bak.tf1.ae_model import ae_model
from utils.utils import generate_batches, update_ema_loss


def main():
    model_name = 'imageAE'
    model_dir = setting.convAE_dir
    model_path = join(model_dir, model_name)

    hparams = setting.hparams

    model = ae_model(**hparams)

    # no that: we generate data during the training ( --> no need to read or split data into train / test / valid set )
    batch_gen = generate_batches(**hparams)

    # loss and sigma^2 (exp weighted average)
    avg_loss, avg_loss_sigma_sq = None, None
    q_ema = 0.01
    best_avg_loss = 9999

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())

        for step in range(hparams['ntraining_steps']):

            # --- training step

            _, loss = sess.run([model['train_op'], model['loss']], feed_dict={model['x']: next(batch_gen)})

            avg_loss, avg_loss_sigma_sq = update_ema_loss(avg_loss, avg_loss_sigma_sq, loss, q_ema)

            # --- print loss

            # note that: there is no need to split into train. and valid. loss as we generate data during the training
            # --> each new training sample is previously unseen

            if step % 100 == 0:
                logger.info('Step {} loss {:1.3} +- {:1.3}'.format(step, avg_loss, np.sqrt(avg_loss_sigma_sq)))

            # --- save tf1

            if step % 500 == 0 and avg_loss < best_avg_loss * 0.99:
                # save the new best tf1
                best_avg_loss = avg_loss

                os.makedirs(model_dir, exist_ok=True)
                model['saver'].save(sess, model_path, global_step=step)
                logger.info('Model saved to ' + model_path)


if __name__ == '__main__':
    main()
