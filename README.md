# README #

In this project a simple autoencoder (AE) is trained on synthetic data generated during the training 
(there is no need to download any files).

There are two stages of training: 1) convolutional AE and 2) stacked AE. 

Convolutional AE encodes images into low resolution feature maps - tensors of shape WxHxF, where (W,H) is image size
and F is number of features.

Stacked AE further encodes low resolution tensors into latent vectors. We compare three variants of stacked AE: 1) 
vanilla AE, 2) variational AE (VAE) and 3) Wasserstein AE (WAE).  


# TODO more detains on VAE, WAE + links to papers / wiki

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact